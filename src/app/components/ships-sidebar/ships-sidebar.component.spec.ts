import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipsSidebarComponent } from './ships-sidebar.component';

describe('ShipsSidebarComponent', () => {
  let component: ShipsSidebarComponent;
  let fixture: ComponentFixture<ShipsSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipsSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipsSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
