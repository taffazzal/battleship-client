import { Component, OnInit, Input } from '@angular/core';
import { BoardService, Board, Tile } from '../../models/board';

@Component({
    selector: 'app-board',
    templateUrl: './board.component.html',
    styleUrls: ['./board.component.scss']
})

export class BoardComponent implements OnInit {
    // public board;
    public rowAlphabets: Array<string> = [];

    private _hoverTiles;

    @Input()
    public set hoverTiles(value) {
        this._hoverTiles = value;
        console.log(this.hoverTiles);
    }

    public get hoverTiles() {
        return this._hoverTiles;
    }

    constructor(
        private service: BoardService
    ) { }

    ngOnInit() {
        this.service.initBoard(10);
        this.getRowAlphabetsRow(this.boards.tiles.length);
        console.log(this.hoverTiles.indexOf('0,5'));

    }

    private getRowAlphabetsRow(length: number) {
        let alphaCode: number = 'A'.charCodeAt(0);
        for (let i = 0; i < length; i++) {
            this.rowAlphabets.push(String.fromCharCode(alphaCode));
            alphaCode++;
        }
    }

    fireArtillery(xCords: number, yCords: number) {
        let tile = this.boards.tiles[xCords][yCords];
        console.log(tile);

        if (!this.checkIfHitIsValid(tile))
            return;

        if (tile.occupied === true) {
            tile.hit = true;
        }
        tile.used = true;

    }

    private checkIfHitIsValid(tile: Tile) {
        if (tile.used) {
            console.log('Already used');
            return false;
        }
        return true;
    }

    // fireTorpedo(e: any): BoardComponent {        
    //     let id = e.target.id,
    //         boardId = id.substring(1, 2),
    //         row = id.substring(2, 3), col = id.substring(3, 4),
    //         tile = this.boards[boardId].tiles[row][col];
    //         console.log(id,boardId,row,col);

    //     if (!this.checkValidHit(boardId, tile)) {
    //       return;
    //     }

    //     if (tile.value == 1) {
    //         this.boards[boardId].tiles[row][col].status = 'win';
    //         //   this.boards[this.player].player.score++;
    //     } else {
    //         this.boards[boardId].tiles[row][col].status = 'fail'
    //     }
    //     // this.canPlay = false;
    //     this.boards[boardId].tiles[row][col].used = true;
    //     this.boards[boardId].tiles[row][col].value = "X";
    //     return this;
    // }

    // checkValidHit(boardId: number, tile: any): boolean {
    //     // if (boardId == this.player) {
    //     //     console.log("Don't commit suicide.", "You can't hit your own board.")
    //     //     return false;
    //     // }
    //     // if (this.winner) {
    //     //     console.log("Game is over");
    //     //     return false;
    //     // }
    //     // if (!this.canPlay) {
    //     //     console.log("A bit too eager.", "It's not your turn to play.");
    //     //     return false;
    //     // }
    //     if (tile.value == "X") {
    //         console.log("Don't waste your torpedos.", "You already shot here.");
    //         return false;
    //     }
    //     return true;
    // }

    get boards(): Board {
        return this.service.getBoards();
    }

}
