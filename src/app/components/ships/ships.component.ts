import { Component, OnInit } from '@angular/core';
import { Ships } from '../../models/ships';
import { UUID } from 'angular2-uuid';

@Component({
    selector: 'app-ships',
    templateUrl: './ships.component.html',
    styleUrls: ['./ships.component.scss']
})
export class ShipsComponent implements OnInit {
    public ships: Ships[] = [];

    constructor(
    ) { }

    ngOnInit() {
        this.createShips();
    }

    private createShips() {
        this.ships.push(
            new Ships(UUID.UUID(), 4, 'Cruiser'),
            new Ships(UUID.UUID(), 2, 'Battleship'),
            new Ships(UUID.UUID(), 5, 'Destroyer'),
        )
        console.log(this.ships);
    }
}
