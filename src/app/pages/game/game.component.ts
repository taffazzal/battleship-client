import { Component, OnInit } from '@angular/core';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { Tile, BoardService, Board } from '../../models/board';

@Component({
    selector: 'app-game',
    templateUrl: './game.component.html',
    styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
    public hoverTiles = [];

    constructor(
        private dragula: DragulaService,
        private service: BoardService
    ) { }

    ngOnInit() {
        const self = this;

        this.dragula.setOptions('bag-ships', {
            copy: (el, source) => {
                return source.id === 'source';
            },
            accepts: function (el: HTMLElement, target: HTMLElement, source, sibling) {
                if (target.id === 'target') {
                    if (+target.dataset.xcords > (self.boards.tiles.length - +el.dataset.length)) {
                        self.hoverTiles = [];
                        return false;
                    }
                    return true;
                }
            },

            copySortSource: false,
            revertOnSpill: true,
            removeOnSpill: false
        });

        this.dragula.drop.subscribe((value) => {
            let [source, target]: HTMLElement[] = value.slice(1);
            // if (target.id === 'target') {
            //     this.setShips(+target.dataset.xcords, +target.dataset.ycords, +source.dataset.length);
            // }
        });

        this.dragula.drag.subscribe((value) => {
            let [source, target]: HTMLElement[] = value.slice(1);

        });

        this.dragula.over.subscribe((value) => {
            let [source, target]: HTMLElement[] = value.slice(1);
            if (target.id === 'target') {
                this.randomShips(+target.dataset.xcords, +target.dataset.ycords, +source.dataset.length);
            }
        });
    }

    randomShips(xCords: number, yCords: number, shipLength: number) {
        setTimeout(() => {
            let length = 10;
            let ranRow = xCords,
                ranCol = yCords;


            // if ((shipLen + ranCol) > len) {
            //     console.log('horizontal not allowed');
            // } else {
            //     for (let i = ranCol; i < (shipLen + ranCol); i++) {
            //         tiles[ranRow][i].occupied = true;
            //     }
            // }
            console.log(ranRow);

            if ((ranRow > 8)) {
                // this.hoverTiles.push({ notAllowed: true });
                console.log('false');
            } else {
                if (this.boards.tiles[xCords][yCords].occupied === true) {
                    // this.hoverTiles.push({ notAllowed: true });
                } else {
                    this.hoverTiles = [];
                    for (let i = ranRow; i < (shipLength + ranRow); i++) {
                        this.hoverTiles.push(`${i},${ranCol}`);
                    }
                }
            }
        });
    }

    setShips(xCords: number, yCords: number, shipLength: number) {
        let length = 10;
        let ranRow = xCords,
            ranCol = yCords;

        // if ((shipLen + ranCol) > len) {
        //     console.log('horizontal not allowed');
        // } else {
        //     for (let i = ranCol; i < (shipLen + ranCol); i++) {
        //         tiles[ranRow][i].occupied = true;
        //     }
        // }

        if ((shipLength + ranRow) > length) {
            console.log('false');
        } else {
            if (this.boards.tiles[xCords][yCords].occupied === true) {
                console.log('already set');
            } else {
                let tiles: Tile[][] = this.boards.tiles;
                for (let i = ranRow; i < (shipLength + ranRow); i++) {
                    tiles[i][ranCol].occupied = true;
                }
            }
        }
    }

    get boards(): Board {
        return this.service.getBoards();
    }

}
