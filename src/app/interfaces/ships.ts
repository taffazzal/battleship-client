export interface IShips {
    _id: string;
    type: 'Cruiser' | 'Destroyer' | 'Submarine' | 'Battleship' | 'Carrier';
    size: number;
}
