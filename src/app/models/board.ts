import { Injectable } from '@angular/core';

export class Board {
    public tiles: Array<Tile[]>;

    constructor(values: { tiles: Array<Tile[]> }) {
        Object.assign(this, values);
    }
}

export interface Tile {
    used: boolean;
    occupied: boolean;
    hit: boolean;
}

@Injectable()

export class BoardService {
    boards: Board[] = [];

    initBoard(boardSize: number = 5) {
        let tiles: Tile[][] = new Array();
        for (let i = 0; i < boardSize; i++) {
            tiles[i] = [];
            for (let j = 0; j < boardSize; j++) {
                tiles[i][j] = { used: false, occupied: false, hit: false } as Tile
            }
        }

        // tiles = this.randomShips(tiles, boardSize);

        let board = new Board({
            tiles: tiles
        })
        console.log(board);

        this.boards.push(board);
    }

    randomShips(tiles: Tile[][], len: number): Tile[][] {
        // len = len - 1;
        // let ranRow = this.getRandomInt(0, len),
        //     ranCol = this.getRandomInt(0, len);
        // if (tiles[ranRow][ranCol].occupied == true) {
        //     return this.randomShips(tiles, len);
        // } else {
        //     tiles[ranRow][ranCol].occupied = true;
        //     return tiles;
        // }
        let shipLen = 1;
        let ranRow = 7,
            ranCol = 3;
        console.log(ranRow, ranCol);

        // if ((shipLen + ranCol) > len) {
        //     console.log('horizontal not allowed');
        // } else {
        //     for (let i = ranCol; i < (shipLen + ranCol); i++) {
        //         tiles[ranRow][i].occupied = true;
        //     }
        // }

        if ((shipLen + ranRow) > len) {
            console.log('vertical not allowed');
        } else {
            for (let i = ranRow; i < (shipLen + ranRow); i++) {
                tiles[i][ranCol].occupied = true;
            }
        }
        console.log(tiles);

        return tiles;
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    getBoards(): Board {
        return this.boards[0];
    }
}

