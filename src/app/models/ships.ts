interface ShipImage {
    full_image: string;
    name: string;
    thumbnail: string;
}

export class Ships {
    private _id: string;
    private _size: number;
    private _type: 'Cruiser' | 'Destroyer' | 'Submarine' | 'Battleship' | 'Carrier';
    private _image: ShipImage;

    public get id(): string {
        return this._id;
    }

    public set id(value: string) {
        this._id = value;
    }

    public get size(): number {
        return this._size;
    }

    public set size(value: number) {
        this._size = value;
    }

    public get type(): 'Cruiser' | 'Destroyer' | 'Submarine' | 'Battleship' | 'Carrier' {
        return this._type;
    }

    public set type(value: 'Cruiser' | 'Destroyer' | 'Submarine' | 'Battleship' | 'Carrier') {
        this._type = value;
    }

    public get image(): ShipImage {
        return this._image;
    }

    public set image(value: ShipImage) {
        this._image = value;
    }

    constructor(id: string, size: number, type: 'Cruiser' | 'Destroyer' | 'Submarine' | 'Battleship' | 'Carrier', image?: ShipImage) {
        this.id = id;
        this.size = size;
        this.type = type;
        this.image = image;
    }

}
