import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { DragulaModule } from 'ng2-dragula';
import { GameComponent } from './pages/game/game.component';
import { ShipsSidebarComponent } from './components/ships-sidebar/ships-sidebar.component';
import { ShipsComponent } from './components/ships/ships.component';
import { BoardComponent } from './components/board/board.component';
import { BoardService } from './models/board';

@NgModule({
    declarations: [
        AppComponent,
        GameComponent,
        ShipsSidebarComponent,
        ShipsComponent,
        BoardComponent
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        DragulaModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [BoardService],
    bootstrap: [AppComponent]
})
export class AppModule { }
